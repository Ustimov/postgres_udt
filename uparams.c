//
//  main.c
//  Uparams
//
//  Created by Artem Ustimov on 3/11/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#include "postgres.h"
#include "fmgr.h"
#include "utils/builtins.h"
//#include <stdio.h>

PG_MODULE_MAGIC;

typedef struct Uparams {
    uint32 kolnii;
    uint32 kolfak;
    uint32 kolstud;
} Uparams;

PG_FUNCTION_INFO_V1(uparams_out);

Datum
uparams_out(PG_FUNCTION_ARGS)
{
    Uparams    *uparams = (Uparams *) PG_GETARG_POINTER(0);
    char       *result;
    
    result = psprintf("(%u,%u,%u)", uparams->kolnii, uparams->kolfak, uparams->kolstud);
    PG_RETURN_CSTRING(result);
}

PG_FUNCTION_INFO_V1(uparams_in);

Datum
uparams_in(PG_FUNCTION_ARGS)
{
    char       *str = PG_GETARG_CSTRING(0);
    uint32 kolnii,
    kolfak,
    kolstud;
    Uparams    *result;
    
    if (sscanf(str, "(%u,%u,%u)", &kolnii, &kolfak, &kolstud) != 3)
        ereport(ERROR,
                (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
                 errmsg("invalid input syntax for uparam: \"%s\"",
                        str)));
    
    result = (Uparams *) palloc(sizeof(Uparams));
    result->kolnii = kolnii;
    result->kolfak = kolfak;
    result->kolstud = kolstud;
    PG_RETURN_POINTER(result);
}

static int
uparams_abs_cmp_internal(Uparams * a, Uparams * b)
{
	double nii_w = 1;
	double fak_w = 0.001;
	double stud_w = 0.000001;

	double a_weight = a->kolnii*nii_w + a->kolfak*fak_w + a->kolstud*stud_w;
	double b_weight = b->kolnii*nii_w + b->kolfak*fak_w + b->kolstud*stud_w;

	if (a_weight < b_weight)
		return -1;
	if (a_weight > b_weight)
		return 1;
	return 0;
}


PG_FUNCTION_INFO_V1(uparams_abs_lt);

Datum
uparams_abs_lt(PG_FUNCTION_ARGS)
{
	Uparams    *a = (Uparams *) PG_GETARG_POINTER(0);
	Uparams    *b = (Uparams *) PG_GETARG_POINTER(1);

	PG_RETURN_BOOL(uparams_abs_cmp_internal(a, b) < 0);
}

PG_FUNCTION_INFO_V1(uparams_abs_le);

Datum
uparams_abs_le(PG_FUNCTION_ARGS)
{
	Uparams    *a = (Uparams *) PG_GETARG_POINTER(0);
	Uparams    *b = (Uparams *) PG_GETARG_POINTER(1);

	PG_RETURN_BOOL(uparams_abs_cmp_internal(a, b) <= 0);
}

PG_FUNCTION_INFO_V1(uparams_abs_eq);

Datum
uparams_abs_eq(PG_FUNCTION_ARGS)
{
	Uparams    *a = (Uparams *) PG_GETARG_POINTER(0);
	Uparams    *b = (Uparams *) PG_GETARG_POINTER(1);

	PG_RETURN_BOOL(uparams_abs_cmp_internal(a, b) == 0);
}

PG_FUNCTION_INFO_V1(uparams_abs_ge);

Datum
uparams_abs_ge(PG_FUNCTION_ARGS)
{
	Uparams    *a = (Uparams *) PG_GETARG_POINTER(0);
	Uparams    *b = (Uparams *) PG_GETARG_POINTER(1);

	PG_RETURN_BOOL(uparams_abs_cmp_internal(a, b) >= 0);
}

PG_FUNCTION_INFO_V1(uparams_abs_gt);

Datum
uparams_abs_gt(PG_FUNCTION_ARGS)
{
	Uparams    *a = (Uparams *) PG_GETARG_POINTER(0);
	Uparams    *b = (Uparams *) PG_GETARG_POINTER(1);

	PG_RETURN_BOOL(uparams_abs_cmp_internal(a, b) > 0);
}

PG_FUNCTION_INFO_V1(uparams_abs_cmp);

Datum
uparams_abs_cmp(PG_FUNCTION_ARGS)
{
	Uparams    *a = (Uparams *) PG_GETARG_POINTER(0);
	Uparams    *b = (Uparams *) PG_GETARG_POINTER(1);

	PG_RETURN_INT32(uparams_abs_cmp_internal(a, b));
}
