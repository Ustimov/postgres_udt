EXTENSION = uparams
DATA = uparams--0.0.1.sql
MODULES = uparams        # the extensions name


# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

