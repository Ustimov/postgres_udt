CREATE TYPE uparams;

CREATE FUNCTION uparams_in(cstring)
   RETURNS uparams
   AS 'MODULE_PATHNAME'
   LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION uparams_out(uparams)
   RETURNS cstring
   AS 'MODULE_PATHNAME'
   LANGUAGE C IMMUTABLE STRICT;

CREATE TYPE uparams (
   INTERNALLENGTH = 12,
   INPUT = uparams_in,
   OUTPUT = uparams_out,
   ALIGNMENT = int4
);


CREATE FUNCTION uparams_abs_lt(uparams, uparams) RETURNS bool
   AS 'MODULE_PATHNAME' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION uparams_abs_le(uparams, uparams) RETURNS bool
   AS 'MODULE_PATHNAME' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION uparams_abs_eq(uparams, uparams) RETURNS bool
   AS 'MODULE_PATHNAME' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION uparams_abs_ge(uparams, uparams) RETURNS bool
   AS 'MODULE_PATHNAME' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION uparams_abs_gt(uparams, uparams) RETURNS bool
   AS 'MODULE_PATHNAME' LANGUAGE C IMMUTABLE STRICT;

CREATE OPERATOR < (
   leftarg = uparams, rightarg = uparams, procedure = uparams_abs_lt,
   commutator = > , negator = >= ,
   restrict = scalarltsel, join = scalarltjoinsel
);
CREATE OPERATOR <= (
   leftarg = uparams, rightarg = uparams, procedure = uparams_abs_le,
   commutator = >= , negator = > ,
   restrict = scalarltsel, join = scalarltjoinsel
);
CREATE OPERATOR = (
   leftarg = uparams, rightarg = uparams, procedure = uparams_abs_eq,
   commutator = = ,
   -- leave out negator since we didn't create <> operator
   -- negator = <> ,
   restrict = eqsel, join = eqjoinsel
);
CREATE OPERATOR >= (
   leftarg = uparams, rightarg = uparams, procedure = uparams_abs_ge,
   commutator = <= , negator = < ,
   restrict = scalargtsel, join = scalargtjoinsel
);
CREATE OPERATOR > (
   leftarg = uparams, rightarg = uparams, procedure = uparams_abs_gt,
   commutator = < , negator = <= ,
   restrict = scalargtsel, join = scalargtjoinsel
);

CREATE FUNCTION uparams_abs_cmp(uparams, uparams) RETURNS int4
   AS 'MODULE_PATHNAME' LANGUAGE C IMMUTABLE STRICT;

-- now we can make the operator class
CREATE OPERATOR CLASS uparams_abs_ops
    DEFAULT FOR TYPE uparams USING btree AS
        OPERATOR        1       < ,
        OPERATOR        2       <= ,
        OPERATOR        3       = ,
        OPERATOR        4       >= ,
        OPERATOR        5       > ,
        FUNCTION        1       uparams_abs_cmp(uparams, uparams);
